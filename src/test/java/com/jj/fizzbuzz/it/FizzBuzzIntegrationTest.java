package com.jj.fizzbuzz.it;

import com.jj.fizzbuzz.entity.FizzBuzzEntity;
import com.jj.fizzbuzz.repository.FizzBuzzRepository;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class FizzBuzzIntegrationTest {

    @Autowired
    FizzBuzzRepository fizzBuzzRepository;
    @LocalServerPort
    private int port;

    @Test
    public void generateFizzBuzzTest() throws JSONException {

        String expected = "[\"1\",\"2\",\"jojo\",\"jiji\",\"5\",\"jojo\",\"7\",\"jiji\",\"jojo\",\"10\",\"11\",\"jojojiji\",\"13\",\"14\",\"jojo\",\"jiji\",\"17\",\"jojo\",\"19\",\"jiji\",\"jojo\",\"22\",\"23\",\"jojojiji\"]";

        String response = given()
                .port(port)
                .queryParam("int1", 3)
                .queryParam("int2", 4)
                .queryParam("limit", 24)
                .queryParam("str1", "jojo")
                .queryParam("str2", "jiji")
                .when()
                .get("fizzbuzz/generate")
                .then()
                .statusCode(200)
                .contentType(JSON)
                .extract()
                .body()
                .asString();

        JSONAssert.assertEquals(expected, response, true);
    }

    @Test
    public void statisticsFizzBuzzTest() throws JSONException {

        String expected = "[{\"int1\": 5,\"int2\": 8,\"limit\": 100,\"str1\": \"jojo\",\"str2\": \"jiji\", \"numberOfHits\": 10}]";

        fizzBuzzRepository.save(createEntity(99, 5, 8, 10));
        fizzBuzzRepository.save(createEntity(55, 3, 4, 5));
        fizzBuzzRepository.save(createEntity(22, 1, 2, 1));

        String response = given()
                .port(port)
                .when()
                .get("fizzbuzz/statistics")
                .then()
                .statusCode(200)
                .contentType(JSON)
                .extract()
                .body()
                .asString();

        JSONAssert.assertEquals(expected, response, true);
    }

    FizzBuzzEntity createEntity(int id, int int1, int int2, int numnberOfHits) {
        FizzBuzzEntity fizzBuzzEntity = new FizzBuzzEntity();
        fizzBuzzEntity.setNumberOfHits(numnberOfHits);
        fizzBuzzEntity.setId(id);
        fizzBuzzEntity.setInt1(int1);
        fizzBuzzEntity.setLimite(100);
        fizzBuzzEntity.setInt2(int2);
        fizzBuzzEntity.setStr1("jojo");
        fizzBuzzEntity.setStr2("jiji");

        return fizzBuzzEntity;
    }

}
