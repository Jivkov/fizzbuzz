package com.jj.fizzbuzz.service;

import com.jj.fizzbuzz.entity.FizzBuzzEntity;
import com.jj.fizzbuzz.mapper.FizzBuzzMapper;
import com.jj.fizzbuzz.model.bo.FizzBuzzBO;
import com.jj.fizzbuzz.repository.FizzBuzzRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FizzBuzzServiceTest {

    @InjectMocks
    FizzBuzzService fizzBuzzService;

    @Mock
    FizzBuzzRepository fizzBuzzRepository;

    @Mock
    FizzBuzzMapper fizzBuzzMapper;

    @Test
    void generateFizzBuzz_Test() {
        // Given
        FizzBuzzBO fizzBuzzBO = new FizzBuzzBO(3, 4, 12, "jojo", "jiji");
        FizzBuzzEntity fizzBuzzEntity = createEntity();
        when(fizzBuzzMapper.fizzBuzzBoToEntity(fizzBuzzBO)).thenReturn(fizzBuzzEntity);

        // When
        List<String> result = fizzBuzzService.generateFizzBuzz(fizzBuzzBO);

        // Then
        verify(fizzBuzzRepository).save(fizzBuzzEntity);
        List<String> expected = List.of("1", "2", "jojo", "jiji", "5", "jojo", "7", "jiji", "jojo", "10", "11", "jojojiji");
        assert (expected).equals(result);
    }

    @Test
    void generateFizzBuzzAlreadyRequested_Test() {
        // Given
        FizzBuzzBO fizzBuzzBO = new FizzBuzzBO(3, 4, 12, "jojo", "jiji");
        FizzBuzzEntity fizzBuzzEntity = createEntity();
        when(fizzBuzzRepository.findById(anyInt())).thenReturn(Optional.of(fizzBuzzEntity));

        // When
        List<String> result = fizzBuzzService.generateFizzBuzz(fizzBuzzBO);

        // Then
        verify(fizzBuzzRepository).save(fizzBuzzEntity);
        List<String> expected = List.of("1", "2", "jojo", "jiji", "5", "jojo", "7", "jiji", "jojo", "10", "11", "jojojiji");
        assert (expected).equals(result);
        assertThat(fizzBuzzEntity.getNumberOfHits()).isEqualTo(2);
    }

    @Test
    void getFizzBuzzStatistics() {
        // Given
        FizzBuzzEntity fizzBuzzEntity = createEntity();

        List<FizzBuzzEntity> list = List.of(fizzBuzzEntity);
        when(fizzBuzzRepository.findAllEntityWithMaxNumberOfHits()).thenReturn(list);
        // When
        fizzBuzzService.getFizzBuzzStatistics();

        // Then
        verify(fizzBuzzMapper).fizzBuzzEntityToDTO(fizzBuzzEntity);
    }


    FizzBuzzEntity createEntity() {
        FizzBuzzEntity fizzBuzzEntity = new FizzBuzzEntity();
        fizzBuzzEntity.setNumberOfHits(1);
        fizzBuzzEntity.setId(10);
        fizzBuzzEntity.setInt1(3);
        fizzBuzzEntity.setLimite(30);
        fizzBuzzEntity.setInt2(4);
        fizzBuzzEntity.setStr1("toto");
        fizzBuzzEntity.setStr2("tata");

        return fizzBuzzEntity;
    }
}