package com.jj.fizzbuzz.controller;

import com.jj.fizzbuzz.model.bo.FizzBuzzBO;
import com.jj.fizzbuzz.service.FizzBuzzService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class FizzBuzzControllerTest {

    @InjectMocks
    FizzBuzzController fizzBuzzController;

    @Mock
    FizzBuzzService fizzBuzzService;

    @Test
    void generateFizzBuzz() {
        // Given

        // When
        fizzBuzzController.generateFizzBuzz(3, 4, 30, "jojo", "jiji");

        // Then
        FizzBuzzBO fizzBuzzBO = new FizzBuzzBO(3, 4, 30, "jojo", "jiji");
        verify(fizzBuzzService).generateFizzBuzz(fizzBuzzBO);
    }

    @Test
    void getFizzBuzzStatistics() {
        // Given

        // When
        fizzBuzzController.getFizzBuzzStatistics();
        // Then
        verify(fizzBuzzService).getFizzBuzzStatistics();
    }
}