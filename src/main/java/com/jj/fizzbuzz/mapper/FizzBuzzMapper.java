package com.jj.fizzbuzz.mapper;

import com.jj.fizzbuzz.entity.FizzBuzzEntity;
import com.jj.fizzbuzz.model.bo.FizzBuzzBO;
import com.jj.fizzbuzz.model.dto.FizzBuzzDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper entre les Enitty les BO et les DTO
 */
@Mapper(componentModel = "spring")
public interface FizzBuzzMapper {

    /**
     * Map un Bo vers un entity
     *
     * @param fizzBuzzBO le BO à convertir
     * @return L'entity correspondant
     */
    @Mapping(target = "limite", source = "fizzBuzzBO.limit")
    @Mapping(target = "id", expression = "java(fizzBuzzBO.hashCode())")
    FizzBuzzEntity fizzBuzzBoToEntity(FizzBuzzBO fizzBuzzBO);

    /**
     * Map un Entity vers un DTO
     *
     * @param fizzBuzzEntity l'entity à covnertir
     * @return le DTO correspondant
     */
    @Mapping(target = "limit", source = "fizzBuzzEntity.limite")
    FizzBuzzDTO fizzBuzzEntityToDTO(FizzBuzzEntity fizzBuzzEntity);
}
