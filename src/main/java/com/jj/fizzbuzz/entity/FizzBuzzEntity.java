package com.jj.fizzbuzz.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Entity representant les paramètres avec lesquels on a appellé le service fizzbuzz
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
public class FizzBuzzEntity {
    @Id
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "int1", nullable = false)
    private int int1;

    @Column(name = "int2", nullable = false)
    private int int2;

    @Column(name = "limite", nullable = false)
    private int limite;

    @Column(name = "str1", nullable = false)
    private String str1;

    @Column(name = "str2", nullable = false)
    private String str2;

    @Column(name = "numberOfHits", nullable = false)
    private int numberOfHits = 1;
}
