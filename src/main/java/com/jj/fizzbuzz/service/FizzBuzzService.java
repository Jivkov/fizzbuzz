package com.jj.fizzbuzz.service;

import com.jj.fizzbuzz.entity.FizzBuzzEntity;
import com.jj.fizzbuzz.mapper.FizzBuzzMapper;
import com.jj.fizzbuzz.model.bo.FizzBuzzBO;
import com.jj.fizzbuzz.model.dto.FizzBuzzDTO;
import com.jj.fizzbuzz.repository.FizzBuzzRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

/**
 * Service FizzBuzz
 */
@Service
public class FizzBuzzService {

    private FizzBuzzRepository fizzBuzzRepository;

    private FizzBuzzMapper fizzBuzzMapper;

    @Autowired
    public FizzBuzzService(FizzBuzzRepository fizzBuzzRepository, FizzBuzzMapper fizzBuzzMapper) {
        this.fizzBuzzRepository = fizzBuzzRepository;
        this.fizzBuzzMapper = fizzBuzzMapper;
    }

    /**
     * Génère la liste des nombres de 1 à limit avec les multiples de int1 remplacés par str1,
     * les multiples de int2 remplacés par str2 et les multiples de int1 et de int2 remplacés par str1+str2
     *
     * @param fizzBuzzBO Objet contenant tous les paramètres venants du controller
     * @return List<String>  La liste des nombres de 1 à limit avec les multiples de int1 remplacés par str1,
     * les multiples de int2 remplacés par str2 et les multiples de int1 et de int2 remplacés par str1+str2
     */
    public List<String> generateFizzBuzz(FizzBuzzBO fizzBuzzBO) {
        updateStatistics(fizzBuzzBO);
        return IntStream.range(1, fizzBuzzBO.getLimit() + 1).mapToObj(nb -> appplyFizzBuzz(nb, fizzBuzzBO)).toList();
    }

    /**
     * Cherche toutes les requêtes enrgistrées qui ont le plus grand nombre d'appel
     *
     * @return la liste des paramètres avec le plus grand nombre d'appel
     */
    public List<FizzBuzzDTO> getFizzBuzzStatistics() {
        List<FizzBuzzEntity> enitytList = fizzBuzzRepository.findAllEntityWithMaxNumberOfHits();
        return enitytList.stream().map(fizzBuzzMapper::fizzBuzzEntityToDTO).toList();
    }

    private String appplyFizzBuzz(int nb, FizzBuzzBO fizzBuzzBO) {
        if ((nb % fizzBuzzBO.getInt1() == 0) && (nb % fizzBuzzBO.getInt2() == 0)) {
            return fizzBuzzBO.getStr1() + fizzBuzzBO.getStr2();
        } else if (nb % fizzBuzzBO.getInt1() == 0) {
            return fizzBuzzBO.getStr1();

        } else if (nb % fizzBuzzBO.getInt2() == 0) {
            return fizzBuzzBO.getStr2();
        }
        return String.valueOf(nb);
    }

    private void updateStatistics(FizzBuzzBO fizzBuzzBO) {
        Optional<FizzBuzzEntity> optEntity = fizzBuzzRepository.findById(fizzBuzzBO.hashCode());
        if (optEntity.isPresent()) {
            FizzBuzzEntity entity = optEntity.get();
            entity.setNumberOfHits(entity.getNumberOfHits() + 1);
            fizzBuzzRepository.save(entity);
        } else fizzBuzzRepository.save(fizzBuzzMapper.fizzBuzzBoToEntity(fizzBuzzBO));
    }
}
