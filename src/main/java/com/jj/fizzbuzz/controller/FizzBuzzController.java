package com.jj.fizzbuzz.controller;

import com.jj.fizzbuzz.model.bo.FizzBuzzBO;
import com.jj.fizzbuzz.model.dto.FizzBuzzDTO;
import com.jj.fizzbuzz.service.FizzBuzzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Controller FizzBuzz
 */
@RestController
@RequestMapping("fizzbuzz")
public class FizzBuzzController {

    private final FizzBuzzService fizzBuzzService;

    @Autowired
    public FizzBuzzController(FizzBuzzService fizzBuzzService) {
        this.fizzBuzzService = fizzBuzzService;
    }

    /**
     * Méthode recevant le get fizzbuzz/generate
     *
     * @param int1  Premier nombre dont il faut remplacer les multiples
     * @param int2  Deuxième nombre dont il faut remplacer les multiples
     * @param limit limite (incluse) de la suite numérique à traiter
     * @param str1  mot avec lequel remplacer les multiples de int1
     * @param str2  mot avec lequel remplacer les multiples de int2
     * @return ResponseEntity<List < String>> la liste des nombres de 1 à limit avec les multiples de int1 remplacés par str1,
     * les multiples de int2 remplacés par str2 et les multiples de int1 et de int2 remplacés par str1+str2
     */
    @GetMapping("/generate")
    @ResponseBody
    public ResponseEntity<List<String>> generateFizzBuzz(
            @RequestParam(name = "int1") @Min(1) @Max(1000000) int int1,
            @RequestParam(name = "int2") @Min(1) @Max(1000000) int int2,
            @RequestParam(name = "limit") @Min(1) @Max(1000000) int limit,
            @NotNull @RequestParam(name = "str1") @Size(max = 64) String str1,
            @NotNull @RequestParam(name = "str2") @Size(max = 64) String str2
    ) {
        return ResponseEntity.ok(fizzBuzzService.generateFizzBuzz(new FizzBuzzBO(int1, int2, limit, str1, str2)));
    }

    /**
     * Renvoie les statistiques de l'application
     *
     * @return La liste des paramètres avec le plus grand nombre d'appel
     */
    @GetMapping("/statistics")
    @ResponseBody
    public ResponseEntity<List<FizzBuzzDTO>> getFizzBuzzStatistics() {
        List<FizzBuzzDTO> result = fizzBuzzService.getFizzBuzzStatistics();
        if (result.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else return ResponseEntity.ok(result);
    }

}
