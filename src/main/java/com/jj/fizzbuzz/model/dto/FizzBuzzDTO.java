package com.jj.fizzbuzz.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO contenant les statistiques de l'application
 */
@Getter
@Setter
public class FizzBuzzDTO {

    private int int1;

    private int int2;

    private int limit;

    private String str1;

    private String str2;

    private int numberOfHits;

}
