package com.jj.fizzbuzz.model.bo;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Business Object contenant les paramètres pour generateFizzBuzz
 */
@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class FizzBuzzBO {

    private int int1;

    private int int2;

    private int limit;

    private String str1;

    private String str2;
}
