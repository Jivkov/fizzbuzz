package com.jj.fizzbuzz.repository;

import com.jj.fizzbuzz.entity.FizzBuzzEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository pour les FizzBuzzEntity
 */
@Repository
public interface FizzBuzzRepository extends JpaRepository<FizzBuzzEntity, Integer> {


    /**
     * Récupère toutes les entrées avec le plus grand nombre d'appel
     *
     * @return la liste des enitity avec le plus grand nombre d'appel
     */
    @Query(value = "FROM FizzBuzzEntity WHERE numberOfHits = (SELECT MAX(numberOfHits) FROM FizzBuzzEntity)")
    List<FizzBuzzEntity> findAllEntityWithMaxNumberOfHits();
}
