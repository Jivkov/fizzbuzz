# Documentation de l'application FizzBuzz

### Description

Cette appliciation permet de génèrer la liste des nombres de 1 à limit(inclut) avec les multiples de int1 remplacés par
str1,
les multiples de int2 remplacés par str2 et les multiples de int1 et de int2 remplacés par str1+str2

### Remarques

* Un swagger est disponible à l'adresse suivante : http://localhost:8080/swagger-ui/index.html
* J'utilise une base de donnée afin de stocker les paramètres de la requête et d'y associé le nombre d'appel afin de
  pouvoir retourner les statistiques.
  Comme il s'agit d'une démo celle-ci n'est présente que dans la mémoire vive et disparait lorsque l'application est
  stoppée.
* J'ai restreint les ints entre 1 et 1000000 et les Strings à une longueur max de 64 caractères afin de ne pas faire
  planter l'application(et même avec ces contraintes, suivant les paramètres demandés, cela peut déjà prendre beaucoup
  de
  temps), cela devrait suffire pour une utilisation normale. (Piste éventuelle : rajouter un circuit-breaker (Hystrix)
  et supprimer les contraintes)
* L'appel aux statistiques renvoie une liste de paramètres car il est possible que plusieurs ensembles de paramètres
  partagent le même nombre d'appels.
* L'applciation a été écrite en java 18


